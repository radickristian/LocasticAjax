( function( $ ) {
		'use strict';
	
		var validateEmail = function( email ) {
	
			// function to validate an email, stolen from chrome
			// http://stackoverflow.com/questions/46155/validate-email-address-in-javascript/46181#46181
	
			var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
			return re.test( email );
		};
	
		var validateForm = function() {
	
			var formValid = true;
	
			var $nameInput = $( '#fname' );
			if ( ! $nameInput.val() ) {
				$nameInput.siblings( '.error-msg' ).text( 'Name must not be empty!' );
				formValid = false;
			} else {
				$nameInput.siblings( '.error-msg' ).text( '' );
			}
	
			var $emailInput = $( '#email' );
			if ( ! validateEmail( $emailInput.val() ) ) {
				$emailInput.siblings( '.error-msg' ).text( 'Email must be a valid email address!' );
				formValid = false;
			} else {
				$emailInput.siblings( '.error-msg' ).text( '' );
			}
	
			var $subjectInput = $( '#subject' );
			if ( ! $subjectInput.val() ) {
				$subjectInput.siblings( '.error-msg' ).text( 'Subject must not be empty!' );
				formValid = false;
			} else {
				$subjectInput.siblings( '.error-msg' ).text( '' );
			}
	
			return formValid;
		};
	
		$( document ).on( 'submit', '#forma', function( event ) {
			event.preventDefault();
	
			// validate form
	
			if ( ! validateForm() ) {
				return;
			}
	
			// send form to server
	
			var posturl = $( this ).attr( 'action' );
	
			$.ajax( {
				url: posturl,
				type: 'POST',
				data: $( this ).serializeArray(),
				dataType: 'text',
				success: function( data, textStatus, jqXHR ) {
					// assuming the server replies 'OK' if everything went okay
					if ( data === 'OK' ) {
						$( '.send-status' ).text( 'Contact request succesfully sent!' );
					} else {
						$( '.send-status' ).text( 'Your contact request was not successfuly sent!' );
					}
				},
				error: function( jqXHR, textStatus, errorThrown ) {
					$( '.send-status' ).text( 'Your contact request was not successfuly sent!' );
				}
			} );
		} );
	}( jQuery ) );
	