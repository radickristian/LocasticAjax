**ROK ZA RJEŠAVANJE ZADATAKA** 10 dana od primitka zadataka.



## 1. Zadatak

Potrebno je napraviti responsive HTML/CSS/JS predložak prema dizajnu koji možete pogeldat na donjim linkovima:

#done
* Desktop:  https://invis.io/CJ814A99E
#done
* Tablet:  https://invis.io/VY814BNHJ
#done
* Mobile:  https://invis.io/8K814CHDJ

## Zahtjevi

### Obavezno
#done
* Izraditi novi repozitorij za zadatak, te redovito commitati i pushati napredak.
#done
* Implementirani HTML/CSS/JS predložak koji se mora se ispravno prikazivati u IE11+, Firefox, Chrome, Opera, Safari.
#done
* Prilagoditi prijelome za Deskop: 1024px, Tablet: 768px, Mobile: 640px po potrebi.
#done
* Pripaziti na prikaz na Retina zaslonima. **HINT** Koristiti svg ili css za sve graficke elemente.
#done
* Čist i pregledan markup, konzistentno imenovanje klasa.
#done
* Modularno korištenje CSS-a pomoću BEM metodologije i OOCSS pristupa **HINT** http://clubmate.fi/oocss-acss-bem-smacss-what-are-they-what-should-i-use/
#done
* Korištenje jednog od prekompajlera za CSS. (SASS, LESS, STYLUS, SUGARSS)
#done
* CSS i JS fileovi se MORAJU kompajlirati iz source foldera u dist folder korištenjem nekog task runnera (Gulp, Grunt, Webpack, NPM skripte...)
#done
* Korištenje sourcemap-a u development enviromentu.
#done 
* Korištenje autoprefiksera i normalize.css-a kao npm dependencia.
#done
* Svi paketi se moraju instalirati preko npm-a i  moraju imati mogućnost updejta bez utjecaja na vaš kod.

### Zabranjeno
#done
* Korištenje CSS frameworka (Bootstrap, Foundation...)
#done
* Korištenje plugina za implementaciju glavnog izbornika.
#done
* Nesting CSS-a viŠe od dvije razine. (osim u slučajevima kada je to neizbježno, primjerice stiliziranje glavne navigacije).

### Dozvoljeno:
#done
* Korištenje jQuery librarya.
#done
* Korištenje plugina za implementaciju slidera.

### Dodatni bodovi:
* Korištenje POSTCSS-a.
#done
* Korištenje NPM skripti za kompajliranje CSS-a i JS-a.
#done
* Korištenje Browserify-a ili Webpacka za kompajliranje JS-a.
#done
* CSS-a i JS-a Linting (Styelint i Standard.js).
#done
* Flex-box grid.
* Korištenje .editorconfiga

### Napomene
  **What**
  Pozadina zaglavlja, podnožja stranice te prvog bloka u kojem se nalazi video proteže se po punoj širini prozora internetskog preglednika.
  #done
  U dostavljenom dizajnu, dizajner je koristio Roboto font.
  #done
  Font je dostupan preko Google Fonts servisa pa ga možete i putem njega implementirati.


## 2. Zadatak

Duplicirajte repozitorij iz prvog zadatka te implementirajte sljedeće:
* Bez korištenja gotovih komponenti napraviti client side-validaciju forme, te ako je forma validna, AJAX POST metodom je submitajte na endpoint www.locastic.com/api/v1/fe-dev
* Očekujemo da rješenje bude u skladu s OOP principima.
* Dodatno se boduje error handling i implementacija AJAX request bez 3rd party libraryja, ali nije obavezno.
* www.slicestic.com/api/v1/fe-dev . Ne postoji I vratiti ce vam 404, bitno je samo pokazati kako šaljete upi

## BONUS ZADACI

  Bonus zadaci nisu obavezni za riješiti, ali kandidati koji ih uspješno riješe imat će prednost u daljnjem postupku natječaja.


## 3. Zadatak

Potrebno je napraviti single-page aplikaciju koristeći Boostrap3 i AngularJS framework.
Aplikacija je jednostavni imenik koji omogućava korisniku unos podataka o kontaktima. Svaki kontakt ima sljedeće podatke:

* `unikatni ID (integer, auto increment)`
* `ime`
* `prezime`
* `adresa`
* `poštanski broj`
* `grad`
* `država`
* `proizvoljan broj telefonskih brojeva`
* `email adresu`

Sva polja su obavezna i moraju se validirati prije pohrane. Storage birajte sami (nema komunikacije sa serverskom stranom). Aplikacija mora moći izlistati sve kontakte (s paginacijom), kreirati, izmijeniti, obrisati i prikazati svaki pojedini kontakt u svom "pageu" (viewu, zasebna ruta za svaki akciju).

Potrebna je mogućnost pretraživanje kontakata po imenu, prezimenu, broju telefona ili e-mail adresi (po nekom ili svim poljima).

Aplikacija nudi korisniku mogućnost merganja kontakata ako oni imaju isto ime i isto prezime. Merge akcija se pokreće na klik gumba. Svaki potencijalni merge mora odobriti korisnik aplikacije. Ako kontakti koji se mergaju imaju popunjene podatke za npr. adresu, onda korisnik mora odabrat koju adresu želi prihvatiti. Ako jedan kontakt ima neki podatak, a ostali u mergu nemaju, onda korisnik ne treba potvrditi merge tog podatka.

Aplikacija mora imati i mogućnost navigsacije sa back/forward mogućnostima browera te mogućnost “shareanja” ili "bookmarkanja" linkova – npr., ako korisnik ode na link .../user/45/edit, mora mu se pokazati forma za uređivanje korisnika sa ID-om 45.


## 4. Zadatak - Security u AngularJS aplikacijama

Razvijate single-page web aplikaciju u AngularJS-u ili nekom drugom JS frameworku. Komunikacija sa serverskom podrškom je pozadinska (AJAX). Da bi korisnik koristio vašu aplikaciju, mora se izvršiti uspješna autentifikacija nakon čega API vraća autorizacijski token (ograničene vremenske valjanosti) koji se koristi za sve ostale API pozive.

1. Kako bi pohranili autorizacijski token u vašoj Angular aplikaciji?
3. Kako biste osigurali sigurnu vezu prema serveru u slučaju da promet ne ide preko HTTPS protokola? Što biste promijenili u slučaju da koristite HTTPS vezu?
4. Kako bi invalidirali token u slučaju isteka njegove valjanosti, a kako u slučaju odjave korisnika (logout)?
5. Koje alternative predlažete za osiguravanje autoriziranog pristupa API-ju osim autorizacijskim tokenom?
6. Prokomentirajte potpisivanje API poziva autorizacijskim tokenom na sigurnoj i nesigurnoj vezi (odlučite se za jedan algoritam i tehniku "digitalnog potpisivanja" zahtjeva).
